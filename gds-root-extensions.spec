%define packaging_base_name 	gds-root-extensions
%define version 3.0.0
%define release 1.1
%define daswg   /usr
%define prefix  %{daswg}
%if %{?_verbose}%{!?_verbose:0}
# Verbose make output
%define _verbose_make 1
%else
%if 1%{?_verbose}
# Silent make output
%define _verbose_make 0
%else
# DEFAULT make output
%define _verbose_make 1
%endif
%endif

%define root_cling %(test ! \\( "`root-config --has-cling`" == "yes" -o "`root-config --version | sed -e 's/[.].*$//g'`" -ge 6 \\); echo $?)

Name: 		gds-root-extensions
Summary: 	gds-root-extensions 3.0.0
Version: 	%{version}
Release: 	%{release}%{?dist}
License: 	GPL
Group: 		LIGO Global Diagnotic Systems
Source: 	https://software.igwn.org/lscsoft/source//%{name}-%{version}.tar.gz
Packager: 	John Zweizig (john.zweizig@ligo.org)
BuildRoot: 	%{_tmppath}/%{name}-%{version}-root
URL: 		http://www.lsc-group.phys.uwm.edu/daswg/projects/dmt.html
BuildRequires: 	gcc, gcc-c++, glibc, automake, autoconf, libtool, m4, make
BuildRequires:  gds-base-devel >= 3.0.0
BuildRequires:  gds-frameio-devel >= 3.0.0
BuildRequires:  gzip
BuildRequires:  bzip2
BuildRequires:  libXpm-devel
BuildRequires:  root
BuildRequires:  readline-devel
BuildRequires:  fftw-devel
BuildRequires:  jsoncpp-devel
Provides:       gds-utilities-root
Obsoletes:      gds-root-base < 2.19.1
Obsoletes:      gds-utilities-root < 2.19.5
Requires:       gds-base-devel >= 3.0.0
Requires:       gds-frameio-devel >= 3.0.0
Requires:       jsoncpp-devel
Prefix:		      %prefix

%description
GDS ROOT wrappers for GDS base libraries

%package -n %{packaging_base_name}-base
Summary: 	Core shared objects
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: gds-base >= 3.0.0
Requires: gds-frameio-base >= 3.0.0

%description -n %{packaging_base_name}-base
GDS ROOT wrappers for GDS base libraries

%package -n %{packaging_base_name}-crtools
Summary: 	Core shared objects
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Obsoletes:      gds-root-crtools < 2.19.1
Requires:       %{packaging_base_name}-base = %{version}-%{release}
Requires:       gds-base-headers >= 3.0.0
Requires:       root

%description -n %{packaging_base_name}-crtools
GDS ROOT wrappers used by some control room tools

%package  -n %{packaging_base_name}-devel
Summary: 	GDS development files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Provides: gds-base-root
Obsoletes: gds-root-devel < 2.19.1
Obsoletes: gds-base-root < 2.19.5
Requires: gds-base-devel >= 3.0.0
Requires: gds-frameio-devel >= 3.0.0

%description -n %{packaging_base_name}-devel
GDS software development files.

%prep
%setup -q

%build
PKG_CONFIG_PATH=%{daswg}/%{_lib}/pkgconfig

%if 0%{?rhel} < 8
CXXFLAGS="-std=c++11"
export CXXFLAGS
%endif

ROOTSYS=/usr

export PKG_CONFIG_PATH ROOTSYS
./configure \
    --prefix=%prefix \
    --libdir=%{_libdir} \
	  --includedir=%{prefix}/include/gds \
	  --enable-online \
    --enable-dtt
make V=%{_verbose_make}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete

%files

%files -n %{packaging_base_name}-base
%defattr(-,root,root)
%{_datadir}/gds/*
%{_libdir}/libgdsevent.so.*
%{_libdir}/libRframeio.so.*
%{_libdir}/libRframefast.so.*
%{_libdir}/libRgdsbase.so.*
%{_libdir}/libRgdsevent.so.*
%{_libdir}/libRgdscntr.so.*
%{_libdir}/libRgdsmath.so.*
%{_libdir}/libRgdstrig.so.*
%{_libdir}/libRxsil.so.*
%if %{root_cling}
%{_libdir}/libRframefast_rdict.pcm
%{_libdir}/libRframeio_rdict.pcm
%{_libdir}/libRgdsbase_rdict.pcm
%{_libdir}/libRgdsevent_rdict.pcm
%{_libdir}/libRgdscntr_rdict.pcm
%{_libdir}/libRgdsmath_rdict.pcm
%{_libdir}/libRgdstrig_rdict.pcm
%{_libdir}/libRxsil_rdict.pcm
%endif

%files -n %{packaging_base_name}-devel
%defattr(-,root,root)
%{_includedir}/*
%{_libdir}/pkgconfig/*
%{_libdir}/*.a
%{_libdir}/*.so

%files -n %{packaging_base_name}-crtools
%defattr(-,root,root)
%{_libdir}/libRdmtsigp.so*
%if %{root_cling}
%{_libdir}/libRdmtsigp_rdict.pcm
%endif

%changelog
* Sat Nov 19 2022 Edward Maros <ed.maros@ligo.org> - 3.0.0-1
- Updated for release as described in NEWS.md

* Wed Nov 02 2022 Edward Maros <ed.maros@ligo.org> - 2.19.5-1
- Updated for release as described in NEWS.md

* Wed Feb 17 2021 Edward Maros <ed.maros@ligo.org> - 2.19.4-1
- Updated for release as described in NEWS.md

* Wed Feb 17 2021 Edward Maros <ed.maros@ligo.org> - 2.19.3-1
- Removed python configuration
- Modified Source field of RPM spec file to have a fully qualified URI

* Tue Feb 02 2021 Edward Maros <ed.maros@ligo.org> - 2.19.2-1
- Corrections to build rules

* Fri Nov 20 2020 Edward Maros <ed.maros@ligo.org> - 2.19.1-1
- Split lowlatency as a separate package
